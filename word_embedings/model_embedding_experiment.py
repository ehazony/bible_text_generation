import random
import matplotlib.pyplot as plt
import numpy as np
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers import LSTM, Dense
from keras.models import Sequential
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.text import text_to_word_sequence
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from word_embedings.my_word2vec import getModel
MIN_WORD_FREQUENCY= 5
SEQUENCE_LEN= 10
tokenizer= Tokenizer()

def get_used_words(tokenizer: Tokenizer):
    words = tokenizer.word_counts.keys()
    ignored_words = set()
    for k, v in tokenizer.word_counts.items():
        if v < MIN_WORD_FREQUENCY:
            ignored_words.add(k)
    print('Unique words before ignoring:', len(words))
    print('Ignoring words with frequency <', MIN_WORD_FREQUENCY)
    words = sorted(set(words) - ignored_words)
    print('Unique words after ignoring:', len(words))
    return words

def dataset_preparation(data):
    global tokenizer
    vec_model = getModel()
    data = data.replace('\xa0', ' \n ')
    text_in_words = text_to_word_sequence(data)
    print('Corpus length in words:', len(text_in_words))
    print('Corpus number of diffrent words:', len(set(text_in_words)))
    # corpus = data.lower().split("\n")
    tokenizer.fit_on_texts([data])
    word_count = tokenizer.word_counts
    total_words = len(tokenizer.word_index) + 1
    ignored_words = set()
    for k, v in word_count.items():
        if v < MIN_WORD_FREQUENCY:
            ignored_words.add(k)

    words = tokenizer.word_counts.keys()
    print('Unique words before ignoring:', len(words))
    print('Ignoring words with frequency <', MIN_WORD_FREQUENCY)
    words = sorted(set(words) - ignored_words)
    print('Unique words after ignoring:', len(words))


    STEP = 1
    sentences = []
    next_words = []
    ignored = 0

    for i in range(0, len(text_in_words) - SEQUENCE_LEN, STEP):
        # Only add sequences where no word is in ignored_words
        if len(set(text_in_words[i: i + SEQUENCE_LEN + 1]).intersection(ignored_words)) == 0:

            sentences.append(text_in_words[i: i + SEQUENCE_LEN])
            next_words.append(text_in_words[i + SEQUENCE_LEN])
        else:
            ignored = ignored + 1
    print('Ignored sequences:', ignored)
    print('Remaining sequences:', len(sentences))
    next_words = tokenizer.texts_to_sequences(next_words)
    word_index = vec_model.wv.vocab
    sentences = [[word_index[t].index for t in seq]
                      for seq in sentences]
    sentences, next_words = shuffle(sentences, next_words)
    sentences,  sentences_test, next_words, next_words_test = train_test_split(sentences, next_words,test_size=0.2, shuffle= True)

    return sentences, next_words, sentences_test, next_words_test, len(tokenizer.word_counts)+1



def create_model(sentences, next_words, sentences_test, next_words_test, total_words):
    model = baseline_model( total_words)
    checkpoint = ModelCheckpoint("best_weghtes_embeding_output.h5", monitor='loss', save_best_only=True)
    sentences =  np.array(pad_sequences(sentences,  maxlen=SEQUENCE_LEN, padding='pre'))
    # sentences_test = np.array(sentences_test)
    sentences_test = np.array(pad_sequences(sentences_test,  maxlen=SEQUENCE_LEN, padding='pre'))
    next_words = np.asarray(next_words).flatten()
    next_words_test = np.asarray(next_words_test)
    history =model.fit(sentences, next_words, validation_data=(sentences_test,next_words_test),
                       epochs=100, verbose=1, callbacks=[checkpoint])
    return model, history


def generate_word(model, tokinzer: Tokenizer, seed_text, diversity):
    """
    genorates a new word given a text sequence
    :return: returns text with adition of the genorated word.
    """
    seed_seq = tokinzer.texts_to_sequences([[s] for s in seed_text])
    seed_token_list = seed_seq
    if(len(seed_seq) < SEQUENCE_LEN):
        seed_token_list = pad_sequences(seed_seq, maxlen=SEQUENCE_LEN, padding='pre')
    elif(len(seed_seq) >= SEQUENCE_LEN):
        seed_token_list = np.asarray(seed_seq[-SEQUENCE_LEN:]).reshape(1,10)
    print('----- diversity:', diversity)
    predicted = model.predict(seed_token_list, verbose=0)[0]
    # number_of_heiest_values = int(predicted.size* diversity)
    number_of_heiest_values = 3
    indexes = predicted.argsort()[-number_of_heiest_values:]
    indx = random.choice(indexes)
    return tokenizer.index_word[indx]

def generate_text(model, tokenizer: Tokenizer,used_words, diversity, len_text):
    # choose seed

    seed = [random.choice(used_words)]
    for i in range(len_text-1+SEQUENCE_LEN):
        seed.append( generate_word(model, tokenizer, seed, diversity))
    return " ".join(seed[SEQUENCE_LEN:])


def baseline_model(total_words):
    model = Sequential()
    embeding = getModel().wv.get_keras_embedding(train_embeddings=False)
    model.add(embeding)
    model.add(LSTM(150, return_sequences=False))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(total_words, activation='softmax'))
    model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()
    return model



def load_base_model(total_words):
    """
    :param num: model qustion to load
    :return: loaded model
    """
    model = baseline_model(total_words)
    model.load_weights('my_model_weights_embeding.h5')
    return model

def save_base_model(modle):
    model.save_weights('my_model_weights_embeding.h5')

def plot_acc(history):
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()

def plot_loss(history):
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()

def generator(sentence_list, next_word_list, batch_size):
    index = 0
    while True:
        yield sentence_list[index:index+batch_size], np.concatenate(next_word_list[index:index+batch_size]).ravel()
        index +=batch_size
        if index>= len(sentence_list)//batch_size:
            index =0

if __name__ == "__main__":
    lines = []
    data = open("new_new_bible_corpus.txt", encoding="utf-8")

    corpus = data.read()
    data.close()
    sentences, next_words, sentences_test, next_words_test, len_words= dataset_preparation(corpus)
    model, history = create_model(sentences, next_words, sentences_test, next_words_test, len_words)
    plot_loss(history)
    plot_acc(history)
    save_base_model(model)
    # tokenizer = Tokenizer()
    # tokenizer.fit_on_texts([corpus])
    # used_words = get_used_words(tokenizer)
    # model = load_base_model(len(used_words)+1)
    # text = generate_text(model, tokenizer,used_words, 0.0001, 1000)
    #
    # with open("generated.txt", 'w', encoding="utf-8") as f:
    #     f.write(text.replace(" \xa0", ".\n"))





