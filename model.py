import random
import matplotlib.pyplot as plt
import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.layers import Embedding, LSTM, Dense
from keras.models import Sequential
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.text import text_to_word_sequence
from sklearn.model_selection import train_test_split

"---------------file parameters-------------------"
MODEL_WEIGHTS_FILE =  "saved_weights\\"+ "more_lstms_weights.h5"
BEST_MODEL_WEIGHTS_FILE = MODEL_WEIGHTS_FILE
CORPUS_FILE  = "corpus_files\\"+ "new_new_bible_corpus.txt"
GENERATED_TEXT_FILE = "generated_full_bible.txt"

"---------------model parameters------------------"
MIN_WORD_FREQUENCY = 3
SEQUENCE_LEN = 15
TRAIN_TEST_SPLIT = 0.2
TEXT_DELIMITER = " ירידתשורה "

tokenizer = Tokenizer()



def dataset_preparation(data):
    """
    prepers data into examples and tags.
    :param data: data corpus
    :return: sentences, next_words, sentences_test, next_words_test, number_of_words
    """
    global tokenizer
    text_in_words = text_to_word_sequence(data)
    print('Corpus length in words:', len(text_in_words))
    print('Corpus number of diffrent words:', len(set(text_in_words)))
    tokenizer.fit_on_texts([data])
    word_count = tokenizer.word_counts
    total_words = len(tokenizer.word_index) + 1
    ignored_words = set()
    for k, v in word_count.items():
        if v < MIN_WORD_FREQUENCY:
            ignored_words.add(k)

    # create used words list
    words = tokenizer.word_counts.keys()
    words = sorted(set(words) - ignored_words)

    # cut the text into sequences of SEQUENCE_LEN words with label witch is the next word
    sentences = []
    next_words = []
    ignored = 0
    for i in range(0, len(text_in_words) - SEQUENCE_LEN):
        # Only add sequences where no word is in ignored_words
        if len(set(text_in_words[i: i + SEQUENCE_LEN + 1]).intersection(ignored_words)) == 0:
            sentences.append(text_in_words[i: i + SEQUENCE_LEN])
            next_words.append(text_in_words[i + SEQUENCE_LEN])
        else:
            ignored = ignored + 1

    print('Unique words before ignoring:', len(words))
    print('Ignoring words with frequency <', MIN_WORD_FREQUENCY)
    print('Unique words after ignoring:', len(words))
    print('Ignored sequences:', ignored)
    print('Remaining sequences:', len(sentences))

    # tern sentences into sequences of numbers (bag of words)
    sentences = tokenizer.texts_to_sequences(sentences)
    next_words = tokenizer.texts_to_sequences(next_words)
    # split into train test
    sentences, sentences_test, next_words, next_words_test = train_test_split(sentences, next_words, test_size=TRAIN_TEST_SPLIT,
                                                                              shuffle=True)
    return sentences, next_words, sentences_test, next_words_test, len(get_words_with_frequency(tokenizer))+1


def create_model(sentences, next_words, sentences_test, next_words_test, total_words):
    """
    creats and trainbase line model
    :return: model and training history
    """
    model = baseline_model(total_words)
    checkpoint = ModelCheckpoint(BEST_MODEL_WEIGHTS_FILE, monitor='loss', save_best_only=True)
    sentences = np.array(pad_sequences(sentences, maxlen=SEQUENCE_LEN, padding='pre'))
    sentences_test = np.array(pad_sequences(sentences_test, maxlen=SEQUENCE_LEN, padding='pre'))
    next_words = np.asarray(next_words).flatten()
    next_words_test = np.asarray(next_words_test)
    history = model.fit(sentences, next_words, validation_data=(sentences_test, next_words_test),
                        epochs=100, verbose=1, callbacks=[checkpoint], batch_size=128)
    return model, history


def generate_word(model, tokinzer: Tokenizer, seed_text, diversity):
    """
    genorates a new word given a text sequence helper for generate_text function.
    """
    seed_seq = tokinzer.texts_to_sequences([[s] for s in seed_text])
    seed_token_list = seed_seq
    if (len(seed_seq) < SEQUENCE_LEN):
        seed_token_list = pad_sequences(seed_seq, maxlen=SEQUENCE_LEN, padding='pre')
    elif (len(seed_seq) >= SEQUENCE_LEN):
        seed_token_list = np.asarray(seed_seq[-SEQUENCE_LEN:]).reshape(1, SEQUENCE_LEN)
    predicted = model.predict(seed_token_list, verbose=0)[0]
    indexes = predicted.argsort()[-diversity:]
    indx = random.choice(indexes)
    return tokenizer.index_word[indx]


def generate_text(model, tokenizer: Tokenizer, used_words, diversity_num, len_text):
    """
    first randomly beulds seed of SEQUENCE_LEN words and then genorates ffrom the sead new text.
    :param model: model to generate from
    :param tokenizer: that whas fit to the data, is needed in order to decode the words
    :param used_words: words that are in the corpus
    :param diversity_num: chouses randmly from the top diversity_num prediction of next words
    :param len_text: length in words of text to generate
    :return: generated text
    """
    seed = [random.choice(used_words)]
    for i in range(len_text - 1 + SEQUENCE_LEN):
        seed.append(generate_word(model, tokenizer, seed, diversity_num))
    return " ".join(seed[SEQUENCE_LEN:])  # returns all but the seed.


def baseline_model(total_words):
    """
    :returns base model for the word generation experiments.
    """
    model = Sequential()
    model.add(Embedding(total_words, input_length=SEQUENCE_LEN, output_dim=SEQUENCE_LEN))
    # model.add(LSTM(150, return_sequences=True))
    # model.add(LSTM(150, return_sequences=True))
    model.add(LSTM(150, return_sequences=False))
    # model.add(Dense(150, activation='relu'))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(total_words, activation='softmax'))
    model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()
    return model



def load_base_model(total_words):
    """
    load model from MODEL_WEGHTS_FILE fo;e total words is the dimension of the word  representation
    :return: loaded model
    """
    model = baseline_model(total_words)
    model.load_weights(MODEL_WEIGHTS_FILE)
    return model


def save_base_model(modle):
    """save model weights to MODEL_WEGHTS_FILE"""
    model.save_weights(MODEL_WEIGHTS_FILE)


def plot_acc(history):
    """base acc plot for a model"""
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()


def plot_loss(history):
    """base loss plot for a model"""
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()


def get_words_with_frequency(tokenizer: Tokenizer):
    """
    getting words with frequency at least MIN_WORD_FREQUENCY
    """
    words = tokenizer.word_counts.keys()
    ignored_words = set()
    for k, v in tokenizer.word_counts.items():
        if v < MIN_WORD_FREQUENCY:
            ignored_words.add(k)
    print('Unique words before ignoring:', len(words))
    print('Ignoring words with frequency <', MIN_WORD_FREQUENCY)
    words = sorted(set(words) - ignored_words)
    print('Unique words after ignoring:', len(words))
    return words


if __name__ == "__main__":
    "----------------training model---------------"
    data = open(CORPUS_FILE, encoding="utf-8")

    # replacing new line with TEXT_DALAMTER in order that new lines will be counted as seperate token.
    corpus = data.read().replace("\n", TEXT_DELIMITER)
    data.close()
    sentences, next_words, sentences_test, next_words_test, len_words= dataset_preparation(corpus)
    model, history = create_model(sentences, next_words, sentences_test, next_words_test, len_words)
    plot_loss(history)
    plot_acc(history)
    save_base_model(model)

    "----------------generating text---------------"
    # data = open(CORPUS_FILE, encoding="utf-8")
    # # replacing new line with TEXT_DALAMTER in order that new lines will be counted as seperate token.
    # corpus = data.read().replace("\n", TEXT_DALAMTER)
    # data.close()
    # tokenizer = Tokenizer()
    # tokenizer.fit_on_texts([corpus])
    # used_words = get_words_with_frequency(tokenizer)
    # model = load_base_model(14658)
    # model.summary()
    # text = generate_text(model, tokenizer, used_words, 1, 5000)
    # with open(GENERATED_TEXT_FILE, 'w', encoding="utf-8") as f:
    #     f.write(text.replace("ירידתשורה", ".\n"))
