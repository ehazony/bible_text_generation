from keras_preprocessing.text import text_to_word_sequence
with open("bible_corpus.txt",encoding="utf-8") as f:
    new_lines = []
    data= f.read()
    data = text_to_word_sequence(data)
    data  = " ".join(data).replace(' \xa0', '\n')
    data = data.split("\n")
    data = [d.strip() for d in data]
    for d in data:
        if d[:2]== 'פ ' or d[:2]== 'ס ':
            d  =d[2:]
    data = [d for d in data if len(d) != 0]
    data = data[:16500]
    data = "\n".join(data)
with open("5_books_corpus.txt", 'w', encoding="utf-8")as f:
    f.write(data)
